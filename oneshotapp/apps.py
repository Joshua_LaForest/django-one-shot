from django.apps import AppConfig


class OneshotappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'oneshotapp'
